import { Component, Input } from '@angular/core';

import { ApiService } from '../api.service';
import { Link } from '../types';

@Component({
  selector: 'hn-link-item',
  templateUrl: './link-item.component.html',
  styleUrls: ['./link-item.component.css']
})
export class LinkItemComponent {

  @Input() link: Link;
  @Input() index: number;

  constructor(
    private api: ApiService
  ) { }

  upvote() {
    this.api.upvote(this.link.id)
      .subscribe(link => this.link = link);
  }
}
