import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { API_URI } from './constant';
import { Link } from './types';

@Injectable()
export class ApiService {
  constructor(
    private http: Http
  ) { }

  getLinks(page: number) {
    return this.http.get(`${API_URI}/link?p=${page}`)
      .map(res => res.json())
      .map(res => Object.assign(res, {pubDate: new Date(res.pubDate)}));
  }

  addLink(link: Link) {
    return this.http.post(`${API_URI}/link`, link)
      .map(res => res.json());
  }

  upvote(id: number) {
    return this.http.post(`${API_URI}/link/${id}/upvote`, { })
      .map(res => res.json());
  }
}
