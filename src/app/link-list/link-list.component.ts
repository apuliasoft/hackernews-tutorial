import { Component, OnInit, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';

import { ApiService } from '../api.service';
import { Link } from '../types';

@Component({
  selector: 'hn-link-list',
  templateUrl: './link-list.component.html',
  styleUrls: ['./link-list.component.css']
})
export class LinkListComponent implements OnInit {
  readonly pageSize = 30;
  links: Link[];
  page: number;

  constructor(
    private api: ApiService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.queryParamMap
      .map(paramMap => paramMap.has('p') ? parseInt(paramMap.get('p'), 10) : 1)
      .startWith(1)
      .do(page => this.page = page)
      .switchMap(page => this.api.getLinks(this.page))
      .subscribe(links => this.links = links);
  }
}
