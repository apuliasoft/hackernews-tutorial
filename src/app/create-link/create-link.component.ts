import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { ApiService } from '../api.service';
import { Link } from '../types';

@Component({
  selector: 'hn-create-link',
  templateUrl: './create-link.component.html',
  styleUrls: ['./create-link.component.css']
})
export class CreateLinkComponent {

  constructor(
    private api: ApiService,
    private router: Router
  ) { }

  createLink(form: NgForm) {
    if (!form.valid) {
      for (const control of Object.values(form.controls)) {
        control.markAsTouched();
      }
    } else {
      this.api.addLink(form.value)
        .subscribe(() => this.router.navigate(['/']));
    }
  }
}
