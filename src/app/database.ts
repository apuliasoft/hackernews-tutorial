import { Link } from './types';

export const DATABASE = [
  {
    id: 1,
    description: 'Revision of Intraluminal Device in Esophagus, Endo',
    url: 'youtu.be',
    pubDate: new Date('2017-12-06T20:02:17Z'),
    points: 142
  },
  {
    id: 2,
    description: 'Removal of Ext Fix from R Ulna, Perc Endo Approach',
    url: 'imdb.com',
    pubDate: new Date('2017-04-20T04:30:22Z'),
    points: 986
  },
  {
    id: 3,
    description: 'Removal of Radioactive Element from Right Eye, Endo',
    url: 'msu.edu',
    pubDate: new Date('2017-05-16T11:28:08Z'),
    points: 386
  },
  {
    id: 4,
    description: 'Reposition Right Spermatic Cord, Open Approach',
    url: 'spiegel.de',
    pubDate: new Date('2017-07-10T01:07:10Z'),
    points: 66
  },
  {
    id: 5,
    description: 'Excision of Uvula, Open Approach',
    url: 'google.com',
    pubDate: new Date('2017-07-01T00:23:54Z'),
    points: 724
  }
] as Link[];
